.. _application-diw:

.. |br| raw:: html

    <br>

=========================
DIW Berlin researchers
=========================

Applications of the DIETER model have been published in a growing number of peer-reviewed articles. Many of these applications were co-authored by DIW Berlin researchers, as the model was initially developed at DIW.

-----------------

Two **companion articles introduce the basic model version**, which we now refer to as `DIETERgms 1.0.2 <https://gitlab.com/diw-evu/dieter_public/dietergms/-/tree/1.0.2>`_, and investigate optimal electrical storage capacity in long-run scenarios with very high shares of renewable energy sources:

* Zerrahn & Schill (2017): Long-run power storage requirements for high shares of renewables: review and a new model, *Renewable and Sustainable Energy Reviews*, 79, 1518-1534. `Paper <https://doi.org/10.1016/j.rser.2016.11.098>`__
* Schill & Zerrahn (2018): Long-run power storage requirements for high shares of renewables: Results and sensitivities, *Renewable and Sustainable Energy Reviews*, 83, 156-171. `Paper <https://doi.org/10.1016/j.rser.2017.05.205>`__ 

-----------------

Another article describes how the GAMS-based core of the model was wrapped in a Python environment, giving rise to DIETERpy:

* Gaete-Morales, Kittel, Roth, Schill (2021): DIETERpy: a Python framework for The Dispatch and Investment Evaluation Tool with Endogenous Renewables. *SoftwareX*, 15, 100784. `Paper <https://doi.org/10.1016/j.softx.2021.100784>`__ 

-----------------

Reduced model versions have been used for more general reflections on the **economics of electrical storage**, its changing role in settings with increasing renewable penetration, and the phenomenon of unintended storage cycling:

* Zerrahn, Schill, & Kemfert (2018): On the economics of electrical storage for variable renewable energy sources. *European Economic Review* 108, 259-279. `Paper <https://doi.org/10.1016/j.euroecorev.2018.07.004>`__; model code available on `Zenodo <https://doi.org/10.5281/zenodo.1170554>`__.
* Schill (2020): Electricity storage and the renewable energy transition. *Joule* 4, 1-6. `Paper <https://doi.org/10.1016/j.joule.2020.07.022>`_; model code available on `Zenodo <https://doi.org/10.5281/zenodo.3935702>`__.
* López Prol & Schill (2021): The Economics of Variable Renewables and Electricity Storage. *Annual Review of Resource Economics*,  13(1), 443-467. `Paper <http://www.annualreviews.org/eprint/WSMW2UJJFWMJXX6FD7PU/full/10.1146/annurev-resource-101620-081246>`__; model code available on `Zenodo <https://doi.org/10.5281/zenodo.4383288>`__.
* Kittel & Schill (2022): Renewable Energy Targets and Unintended Storage Cycling: Implications for Energy Modeling. *iScience*, 25(4), 104002. `Paper <https://doi.org/10.48550/arXiv.2107.13380>`__; model code available `here <https://gitlab.com/diw-evu/dieter_public/dieterpy_reduced>`__.

-----------------

Other model applications have explored power sector effects of **solar prosumage in Germany and Western Australia**:

* Schill, Zerrahn & Kunz (2017): Prosumage of solar electricity: pros, cons, and the system perspective. *Economics of Energy and Environmental Policy* 6(1), 7-31. `Paper <https://doi.org//10.5547/2160-5890.6.1.wsch>`__; model version `DIETERgms 1.2.0 <https://gitlab.com/diw-evu/dieter_public/dietergms/-/tree/1.2.0>`__.
* Say, Schill & John (2020): Degrees of displacement: The impact of household PV battery prosumage on utility generation and storage. *Applied Energy* 276, 115466. `Paper <https://doi.org/10.1016/j.apenergy.2020.115466>`__; model code available on `Zenodo <https://doi.org/10.5281/zenodo.3693286>`_.

-----------------

To analyze the effects of **tariff design on prosumage decisions**, a model version with a mixed complementarity problem (MCP) instead of a linear optimization problem was developed and applied to Germany:

* Günther, Schill, & Zerrahn (2021): Prosumage of solar electricity: tariff design, capacity investments, and power sector effects. *Energy Policy* 152, 112168. `Paper <https://doi.org/10.1016/j.enpol.2021.112168>`__; model code available on `Zenodo <https://doi.org/10.5281/zenodo.3345783>`__.

-----------------

Another model application explored the power sector impacts of **electric vehicles in Germany**, focussing on reserve provision and vehicle-to-grid:

* Schill, Niemeyer, Zerrahn & Diekmann (2016): Bereitstellung von Regelleistung durch Elektrofahrzeuge: Modellrechnungen für Deutschland im Jahr 2035. *Zeitschrift für Energiewirtschaft* 40 (2), 73-87. `Paper <http://dx.doi.org/10.1007/s12398-016-0174-7>`__; model version `DIETERgms 1.1.0 <https://gitlab.com/diw-evu/dieter_public/dietergms/-/tree/1.1.0>`__.

-----------------

Another paper introduced a module for **residential power-to-heat options** and an application focussing on the flexibilization of legacy night-time storage heaters, using model version `DIETERgms 1.3.0 <https://gitlab.com/diw-evu/dieter_public/dietergms/-/tree/1.3.0>`__:

* Schill & Zerrahn (2020): Flexible electricity use for heating in markets with renewable energy. *Applied Energy* 266, 114571. `Paper <https://doi.org/10.1016/j.apenergy.2020.114571>`__

-----------------

A **hydrogen module** was introduced and applied to explore the trade-off between energy efficiency and temporal flexibility of various green hydrogen supply chains:

* Stöckl, Schill & Zerrahn (2021): Optimal supply chains and power sector benefits of green hydrogen. *Scientific Reports* 11, 14191. `Paper <https://doi.org/10.1038/s41598-021-92511-6>`__; model code available on `Zenodo <https://doi.org/10.5281/zenodo.3693305>`__.

-----------------

A reduced model version has also been used to generate data points for estimating the macroeconomic **elasticity of substitution between "clean" and "dirty" electricity generation** for high shares of renewables not yet observable in empirical data:

* Stöckl & Zerrahn (2020): Substituting Clean for Dirty Energy: A Bottom-Up Analysis. *DIW Discussion Paper*. `Paper <https://www.diw.de/documents/publikationen/73/diw_01.c.795779.de/dp1885.pdf>`__; model code available on `Zenodo <https://zenodo.org/record/3940514#.YFOj469Kg2w>`__.

-----------------

The model has also been used in the FlexMex model comparison exercise, which has led to three publications:

* Gils, Gardian, Kittel, Schill, Zerrahn, Murmann, Launer, Fehler, Gaumnitz, van Ouwerkerk, Bußar, Mikurda, Torralba-Díaz, Janßen, Krüger (2022): Modeling flexibility in energy systems — comparison of power sector models based on simplified test cases. *Renewable and Sustainable Energy Reviews*. 158, 111995. `Paper <https://doi.org/10.1016/j.rser.2021.111995>`__

* van Ouwerkerk, Gils, Gardian, Kittel, Schill, Zerrahn, Murmann, Launer, Torralba-Díaz, Bußar (2022): Impacts of power sector model features on optimal capacity expansion: A comparative study. *Renewable and Sustainable Energy Reviews*, 157, 112004. `Paper <https://doi.org/10.1016/j.rser.2021.112004>`__

* Gils, Gardian, Kittel, Schill, Murmann, Launer, Gaumnitz, van Ouwerkerk, Mikurda, Torralba-Díaz (2022): Model-related outcome differences in power system models with sector coupling—Quantification and drivers. *Renewable and Sustainable Energy Reviews*, 159, 112177. `Paper <https://doi.org/10.1016/j.rser.2022.112177>`__

-----------------

Work in progress (selected):

* A contribution to an open-source model comparison on the effects of cheaper stationary batteries, using the model version `DIETERgms 1.3.1 <https://gitlab.com/diw-evu/dieter_public/dietergms/-/tree/1.3.1>`__
* An evaluation of the power sector implications of different types of low-carbon freight traffic
* A detailed analysis of the trade-off between additional demand and additional flexibility potential related to battery-electric vehicles
* An application to Europe to analyse how electricity storage needs are affected by geographical balancing
