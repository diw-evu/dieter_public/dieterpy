.. |br| raw:: html

    <br>

************************
Team
************************

The team of DIETER core developers is a group of Post-Docs and Ph.D. students at DIW Berlin. If you want to contribute to the development of DIETER, supply new input data, share your research based on DIETER, or are interested in collaborating with us, please do not hesitate to contact_ us.

.. _contact: wschill@diw.de

Current Developers
========================

*Wolf-Peter Schill*
""""""""""""""""""""

.. image:: https://www.diw.de/sixcms/media.php/37/thumbnails/WSchill.jpg.568394.jpg
      :width: 150

Wolf is Deputy Head of the Department Energy, Transportation, Environment at DIW Berlin and leads the group Transformation of the Energy Economy. He is interested in various aspects of the renewable energy transformation. Together with Alexander Zerrahn, he is one of the fathers of the DIETER model. He made substantial contributions to the initial model development, different types of model extensions, and various applications.

**DIETER Expertise:** |br|
+ A bit of everything

**E-Mail:** wschill@diw.de |br| **Work:** `Google Scholar <https://scholar.google.com/citations?user=Y6aa6xgAAAAJ&hl=de&oi=sra>`__

|

*Carlos Gaete-Morales*
"""""""""""""""""""""""

.. image:: https://www.diw.de/sixcms/media.php/37/thumbnails/CGaete.jpg.574923.jpg
  :width: 150

Carlos is a research associate at DIW Berlin, holds a PhD from The University of Manchester and an Industrial Engineering degree. His research focuses on the sustainability assessment of energy systems and finding sustainable pathways for future energy economy by implementing optimization models, life cycle assessment (LCA), and machine learning techniques. He is interested in outreach his work by developing open-source tools using Python and is the father of the Python wrapper for DIETER, that is DIETERpy.

**DIETER Expertise:** |br|
+ Model analyses of settings with high renewables |br|
+ e-Mobility, see also `emobpy <https://pypi.org/project/emobpy>`_ |br|
+ Energy storage |br|

**E-Mail:** cgaete@diw.de |br| **Work:** `Google Scholar <https://scholar.google.com/citations?user=Cay15a0AAAAJ&hl=en&oi=ao>`__

|

*Alexander Roth*
""""""""""""""""""

.. image:: https://www.diw.de/sixcms/media.php/37/thumbnails/ARoth.jpg.551959.jpg
  :width: 150

Alex is a research associate and PhD candidate at DIW Berlin. He works currently on a multi-country version of DIETERpy and the interaction effects of electricity storages.

**DIETER Expertise:** |br|
+ Multi-country applications |br|
+ Electricity storage

**E-Mail:** aroth@diw.de |br| **Work** `Google Scholar <https://scholar.google.de/citations?hl=de&user=WF4xfL4AAAAJ>`__

|

*Martin Kittel*
""""""""""""""""""

.. image:: https://www.diw.de/sixcms/media.php/37/thumbnails/MKittel.jpg.551922.jpg
  :width: 150

Martin is a research associate and PhD candidate at DIW Berlin. He mainly develops and maintains our stylized DIETERpy model, and renewable energy constraints in DIETERpy. In his current work, Martin investigates the impact of renewable energy constraints on power sector and energy system models.

**Expertise:** |br|
+ Renewable energy constraints |br|
+ Power-to-power storage |br|
+ Cluster analysis

**E-Mail:** mkittel@diw.de |br| **Work:** `Google Scholar <https://scholar.google.com/citations?user=wpZdqusAAAAJ&hl=de&oi=sra>`__

|

Past Developers
========================

*Alexander Zerrahn*
""""""""""""""""""""

.. image:: https://www.diw.de/sixcms/media.php/37/thumbnails/AZerrahn.jpg.546210.jpg
  :width: 150

Alexander was a research associate at DIW Berlin until December 2020 and is, together with Wolf-Peter Schill, one of the fathers of the DIETER model. He made substantial contributions to the initial model development and various model applications.

**E-Mail:** azerrahn@diw.de |br| **Work:** `Google Scholar <https://scholar.google.at/citations?user=cPPu_1QAAAAJ&hl=de>`__

|

*Fabian Stöckl*
""""""""""""""""""

.. image:: https://www.diw.de/sixcms/media.php/37/thumbnails/FStoeckl.jpg.546586.jpg
  :width: 150

Fabian was a research associate at DIW Berlin until May 2021. He mainly developed and maintained our hydrogen module, including the Hydrogen-for-Mobility, the Power-to-Gas, and the Power-to-Liquid branches.

**Expertise:** |br|
+ Hydrogen |br|
+ P2X

**E-Mail:** fstoeckl@diw.de |br| **Work:** `Google Scholar <https://scholar.google.com/citations?user=aPcI00QAAAAJ&hl=de&oi=sra>`__

|

*Claudia Günther*
""""""""""""""""""

As a working student and for her master thesis, Claudia developed a model version in a mixed complementarity problem (MCP) format. She used this MCP-version of DIETER to explore the effects of tariff design on prosumage decisions. For this work, which also led to a peer-reviewed article in `Energy Policy <https://doi.org/10.1016/j.enpol.2021.112168>`_, Claudia received the GEE price for the best master thesis in 2019.
